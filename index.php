<? 
$pytimeapi="192.168.5.76:8085";
$gotimeapi="192.168.5.74:8086";

function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

echo "<br><BR><center>";
echo "<table cellspacing=5 cellpadding=2 border=2>";
echo "<TR><TD><center><b><h2> &nbsp; &nbsp; &nbsp; pytimeapi &nbsp; &nbsp; &nbsp; </h2></b></center></td>";
echo "<td><center><b><h2> &nbsp; &nbsp; &nbsp; gotimeapi &nbsp; &nbsp; &nbsp; </h2></b></center></td>";
echo "<Tr><Td><center><br><h2>$pytimeapi</h2><br> </center></td>";
echo "<Td><center><br><h2>$gotimeapi</h2><br> </center></td>";
echo "</table></center>";

?>
